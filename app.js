// Imports
const express = require('express')
const app = express()
const port = 3001
const views = ['index', 'pages/game']

// Static Files
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/js', express.static(__dirname + 'public/js'))
app.use('/img', express.static(__dirname + 'public/img'))

// Zurb Foundation
app.use('/css', express.static(__dirname + 'public/foundation/css'))
app.use('/js', express.static(__dirname + 'public/foundation/js'))

// Set Views
app.set('views', './views')
app.set('view engine', 'ejs')

//get Index

app.get('/', (req, res) => {
    res.render('index')
})

// Get Views

views.forEach(function(value, index) {
    app.get('/' + value, (req, res) => {
        res.render(value)
    })
});


// Listen on port 3001
app.listen(port, () => console.info(`Serwer został uruchomiony na port: ${port}.`))