var cards = [];

var pointsAmount = 0;

var movesAmount = 0;

var clicks = 0;


document.addEventListener('DOMContentLoaded', function() {
    
    //licznik punktów
    document.getElementById('pointsCount').innerHTML = pointsAmount;

    //Licznik ruchów
    document.getElementById('movesCount').innerHTML = movesAmount;

    //generowanie kart

    const activateBtn = document.querySelector('.button');

    activateBtn.addEventListener('click', function() {
        let container = document.getElementById('cards_pool');
        var toAdd = document.createDocumentFragment();
        for(var i = 1; i <= 16; i++) {
            saveCard(i);

            var newDiv = document.createElement('div');
            newDiv.id = 'r' + i;
            newDiv.className = 'card';
            newDiv.style.background = 'white';
            newDiv.onclick = event => triggerCard(event.target);

            toAdd.appendChild(newDiv);
        }
     
        container.appendChild(toAdd);        
    })

    //zabezpieczenie przycisku start
    document.getElementById("btn").addEventListener("click", hideButton);

    function hideButton() {
        document.getElementById("btn").style.visibility = 'hidden';
    }
})



/*
Co posiada Card?
    currentCard = [
        {
            id: 1,
            couple: 2,
            color: jego kolor,
            show: czy jest widoczna,
            found: czy zostala znaleziona,
        },
    ];
*/

function saveCard(i) {
    let color = ['red', 'blue', 'green','orange', 'yellow', 'black', 'purple', 'pink'];

    if(i % 2 === 0) {
        cards.push({
            id: 'r' + i,
            couple: 'r' + parseInt(i - 1),
            color: color[(i / 2) - 1],
            show: false,
            found: false,
            
        });
    }
    else {
        cards.push({
            id: 'r' + i,
            couple: 'r' + parseInt(i + 1),
            color: color[(i - 1) / 2],
            show: false,
            found: false,
        });
    }
}

function getCard(currentCardId) {
    return cards.find(card => card.id === currentCardId);
   
}

function changeColor(currentCard, event) {
    if(currentCard.show) {
        event.style.background = 'white';
        currentCard.show = false;
        addMoves();
        clicks += 1;
    } else {
        event.style.background = currentCard.color; // ustaw kolor
        currentCard.show = true;
        addMoves();
        clicks += 1;
    }
}
//licznik ruchów
function addMoves(){
    movesAmount++
    document.getElementById('movesCount').innerHTML = movesAmount;
}
//licznik punktów
function addPoints(){
    pointsAmount++
    document.getElementById('pointsCount').innerHTML = pointsAmount;
}



function hideCard(currentCard, event, coupleCard){
    if(currentCard.show && coupleCard.show) {
        event.style.visibility = 'hidden';
        document.getElementById(coupleCard.id).style.visibility = 'hidden';
        currentCard.found = true;
        coupleCard.found = true;
            addPoints();
            checkFinish();
            
    }
}


function triggerCard(event) {
    let currentCard = getCard(event.id); // pobierz obiekt tej karty
    let coupleCard = getCard(currentCard.couple);
    changeColor(currentCard, event);
    makeWhite(currentCard, event, coupleCard);
}

//zmiana na biały w przypadku nie odpowiednich kolorow
function makeWhite(currentCard, event, coupleCard){
    if(currentCard.show && coupleCard.show){
        hideCard(currentCard, event, coupleCard)
        clicks = 0;
        
    }else if (clicks % 2 === 0 && currentCard.show){
        checkShow();
        
    
    } 
        
}

function checkShow(){
    setTimeout(function(){
        cards.forEach((e) => {
            if(e.show){
                e.show = false;
                document.getElementById(e.id).style.background = 'white';
            }
        })
    }, 500)
    
    
}

function checkFinish(){
    setTimeout(function(){
    if(pointsAmount === 8){
        confirmWinning();
    }
}, 3000)
}

function confirmWinning(){
    if (confirm("Gratulacje!")) {
        location.reload();
    } else {
        location.reload();  
    }
}







