Wpisz `npm install` by zainstalować node_modules

By urochomić serwer wpisz `npm start`, w consoli powinno być napisane w którym porcie powinien być serwer

Przykład: wpisz w przeglądarce `localhost:3001/`

Żeby dodać nowy plik html wejdź do app.js i zejdź do komentarza `Imports` 
W views masz tablice z nazwami dopisuj przecinek i w nawiasch wpisz nazwę folderu
Ważne jest aby nowy html miał końcówkę "ejs"

Przykład:

Utworzyłem plik `index.ejs` w views, następnie w app.js do tablicy `const views` dopisuje nazwę czyli `const views = ['','about','index']`

W public mamy pliki js - javascript, css, img - zdjęcia, foundation - framework

Korzystamy z tego FrameWork: `https://get.foundation/`